---
title: Konzepte
sidebar: auto
#sidebarDepth: 0
---

# {{ $page.title }}

Die Bereitstellung von Infrastruktur und die Konfiguration von Systemen kann ein Computer schnell und zuverlässig
durchführen. Ein Mensch benötigt mehr Zeit, die Fehlerquote steigt. Die Arbeit kann dadurch als stressig, unbefriedigend
und unkreativ empfunden werden.

Der Infrastruktur as Code(IAC) und Configuration Management as Code(CMC) Ansatz biete hierfür eine Alternative.
Computer machen was sie am besten können (automatisieren) und die Menschen tun was sie am besten können (declare, code).

Gegenüber dem zusammenklicken in einer Oberfläche oder dem manuellen Ausführung von Kommandos hat der IAC und CMC Ansatz
eine Reihe von Vorteile:

- Höhere Zuverlässigkeit der Bereitstellung. Risiko manueller Bedien- oder Tippfehler lässt sich reduzieren.

- Selbst komplexe Änderungen lassen sich nach der Erstellung des Codes binnen kürzester Zeit durchführen.

- Änderungen der Ressourcen sind zu beliebigen Zeiten unabhängig vom verfügbaren Personal automatisiert durchführbar.

- Downtimes der Infrastruktur während der Änderungen oder Anpassungen reduzieren sich.

- Die benötigten Infrastrukturkomponenten und deren Konfiguration sind im Code beschrieben und befindet sich nicht im
Kopf des Administrators oder einer im schlimmsten Fall nicht mehr aktuellen Dokumentation.

- Ist über die Versionsverwaltung historisiert und bietet dadurch die Möglichkeit auf einen früheren Stand zurückzugehen.
Die Änderungen sind über die commit Historie nachvollziehbar und über die commit Kommentare dokumentiert.

- Kann durch eine Review Prozess verifiziert und automatisch getestet werden. Quality Gates können die Übergänge
(dev -> stage -> prod) absichern.

- Kann modularisiert und wiederverwendet werden. Einmal erstellter Programmcode kann beliebig oft wiederverwendet werden.

- Infrastrukturen lassen sich beliebig duplizieren - beispielsweise für Betriebs-, Staging- oder Testumgebungen.

- Tools ermöglichen die anbieterübergreifende Programmierung von Infrastrukturleistungen mit gleicher Programmiersprache.

Ergänzt durch die Containervirtualisierung lassen sich so automatisiert Umgebungen bereitstellen die den genannten
Anforderungen gerecht werden.

Mit diesen Ansätzen lässt sich das Konzept von
[Immutable Infrastructure](https://www.digitalocean.com/community/tutorials/what-is-immutable-infrastructure) umsetzen.

Durch eine engere Verknüpfung von Betrieb und Softwareentwicklung (DevOps) verkürzen sich Entwicklungsprozesse und die
Zusammenarbeit der Prozessbeteiligten verbessert sich.

## Infrastruktur as Code

> A long time ago, in a data center far, far away, an ancient group of powerful beings known as sysadmins used to
deploy infrastructure manually. Every server, every route table entry, every database configuration, and every load
balancer was created and managed by hand. It was a dark and fearful age: fear of downtime, fear of accidental
misconfiguration, fear of slow and fragile deployments, and fear of what would happen if the sysadmins fell to the
dark side (i.e. took a vacation). The good news is that thanks to the DevOps Rebel Alliance, we now have a better way
to do things: Infrastructure-as-Code (IAC). [Quelle: Terraform Up and Running by Yevgeniy Brikman](https://www.oreilly.com/library/view/terraform-up-and/9781491977071/preface01.html)

![iac](./images/iac.png)

Zur Bereitstellung von Infrastrukturleistungen müssen keine manuellen Konfigurationsarbeiten ausgeführt werden. Die
Infrastruktur wird deklarativ beschrieben und automatisiert gemäß den Vorgaben konfiguriert. Das Erstellen der
Infrastruktur ist ein mit dem Programmieren von Software vergleichbarer Vorgang.

Beim Aufbau einer Infastructure as Code Umgebung werden wiederverwendbare Module erstellt. Um den Aufbau einer
Umgebung zu automatisieren werden diese dann miteinander kombinert, für den konkreten Fall konfiguriert und
individualisiert.

Für den Aufbau und die Pflege bieten sich der klassischen Entwicklungsworkflows Code -> Test -> Deploy an. Die Ergebnisse
können dann versioniert und dokumentiert werden.

Zum Einsatz kommt eine Configuration Management Database (CMDB). Diese hält an zentraler Stelle die Werte für die Geräte,
Anwendungen, Datenbanken usw. der gesamten Umgebung vor. Änderungen der Werte werden durch die Tools in die CMDB
zurückgeschrieben, um diese aktuell zu halten.

## Configuration Management as Code

> DevOps has several components that must work in unison for a team to meet its objectives. A key element, which usually
serves as the center of the DevOps "machinery," is configuration management as code. (CMC)

![cm](./images/cm.png)

Die Konzepte und das Vorgehen entsprechen dem IAC Ansatz.
Das Configuration Management ist damit der "Single point of truth" für die Beschreibung des Zielsystems.
Zur Laufzeit des Configuration Management Tools wird dann diese deklarative Beschreibung zur Ausführung gebracht, um das
System in seinen Zielzustand zu bringen.

Der Entwicklungsworkflow ist identisch mit dem IAC Entwicklungsworflow.

## Containervirtualisierung

> Containervirtualisierung (oder: Containering) ist eine Methode, um mehrere Instanzen eines Betriebssystems
(als sog. "Gäste") isoliert voneinander auf einem Hostsystem zu betreiben. Im Gegensatz zur Virtualisierung mittels
eines Hypervisors hat Containervirtualisierung zwar einige Einschränkungen in der Art ihrer Gäste, gilt aber als
besonders ressourcenschonend. Populär in der IT wurde ab 2013 besonders die Software Docker, auch wenn es davor schon
diverse andere Projekte gab, die Vergleichbares realisierten. Charakterisierend für Container ist, dass Gäste den Kernel
des Host-Betriebssystems nutzen. [Quelle Wikipedia](https://de.wikipedia.org/wiki/Containervirtualisierung)

Das folgende Schaubild zeigt die Arbeitsweise für die Erstellung und Verwaltung und das Betreiben von Container Images
und Container:

![iteos_devops_docker](./images/iteos_devops_docker.png)

Es werden die folgenden Schritte durchlaufen:

1. Definition des Conatiners auf dem Entwuicklungsrechner in einem Dockerfile. Code -> Test -> Deploy lokal

2. Festschreiben der Änderungen (git commit) in der Versionsverwaltung und Veröffentlichen der Ändrungen (git push) auf dem GIT Server.

3. Das Veröffentlichen der Änderungen am Code Repository triggered das CI System und veranlasst die Erstellung (docker build)
des Container Image.

4. Ausführen von Test durch das CI System

5. Das erstellte und verifizierte Image wird in der zentralen Docker Registry abgelegt (docker push)

6. Systeme, die einen Container auf Basis eines so erstellten Images betreiben wollen holen sich das Image aus der
zentralen Registry und starten eine Container Instanz (docker run). Das Starten der Instanz wird entsprechend parametrisert.
Die Orchestrierung mehrere Services als Docker Conatiner auf einem Host erfolgt durch den Einsatz von Docker Compose.

Nach erfolgter Conatinerisierung aller Services können diese durch das Configuration Management in der Umgebung auf ihren
Zielsystemen ausgebracht werden. Dazu "installiert" das Configuration Managemnt Tool die Stackbeschreibung die in Form
von Docker Compose Dateien vorliegt auf dem Zielsystem. Das Starten aller Services erfolgt dann über ein einzelnes
Kommando (docker-compose up).
Alle laufzeitrelevanten Dienste (Application Server, Middlewar, Datenbanken, ...) stehen dann zur Verfügung. Die
fachliche Anwendung und ihre Besatandteile (Java EAR Archive, Datenbank Schemata und Inhalte, LDAP Daten, ...) können dann
durch das zugehörige CI System bereitgestellt werden.

Ab einer gewissen Größenordnung ist dieses Vorgehen nicht mehr praktikabel. Die Orchestrierung und die Verwaltung der
Services / Container erfolgt zwar automatisch kann aber durch den Einsatz spezialisierter Container Orchestrierungs
Werkzeuge (Kubernetes) und Cluster Management Lösungen (Rancher) vereinfacht und vereinheitlicht werden.
Die Etablierung dieser Tools in die bestehende Infrastruktur befindet sich aktuell im Aufbau und wird der nächste große
Meilenstein in der Infrastruktur Automatisierung der AE Systeme. Mittel und langfristig sollen alle Services die
momentan auf unabhängigen Docker Hosts betrieben werden unter die Verwaltung von Kubernetes gestellt werden. Dies gilt
sowohl für die allgemeinen Infrastruktur Dienste wie auch die Anwendungs-Stages der Fachverfahren.

Als letzter Schritt ist geplant, den Kollegen aus den Entwicklungsteams der Fachverfahren die Möglichkeit zu bieten,
die von ihnen durchgeführten Codeänderungen direkt auf dedizierten Bereichen des Kubernetes Clusters betreiben zu können.

Wir erhoffen uns dadurch einen langersehnten Wunsch zu erfüllen: Aller läuft in derselben Umgebung egal ob Entwicklung,
Ci, QS (oder irgendwann auch mal Produktion?).

> Das Vorhalten lokaler Installationen auf den Arbeitsplatzrechner der Entwickler und "Works on my machine..." war gestern.