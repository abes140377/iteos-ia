---
title: Step 2 - Bootstrap Host
---

# {{ $page.title }}

![infra-step-2u3](./images/infra-step-2u3.png)

::: tip
Der Bootstrap Schritt muss nur einmal ausgeführt werden. Nur bei der Änderung der Rolle der Maschine ist das wiederholte
Ausführen notwendig. Eine Mehrfachausführung ist unkritisch. Der Schritt ist idempotent.
:::

## Kommando

Host wählen der ge-bootstrapped werden soll.

``` bash
yade host list
```

Bootstrap ausführen

``` bash
yade host bootstrap <hostname>.<domain_name>
```

## Ablauf

- Daten der Maschine werden geladen
- Hostname wurde gesetzt bzw. geprüft
- Namensauflösung wurde eingerichtet bzw. geprüft
- Update des Paketmanagers wurde durchgeführt
- Base Packages wurden installiert (optional)
- Installation des Configuration Management Providers (Chefdk / Ansible Installation)
- Bootstrap Repository wurde ausgeckeckt und initialisiert (GIT clone, Installation Cookbooks / Playbooks)
- Die für das spätere Provisioning erforderliche Rolle der Maschine wurde festgelegt
- Abhängig von der Bootstrap Rolle der Maschine wird die Grundkonfiguration vorgenommen (Chef Solo)

## Ergebnis

Die grundlegende Konfiguration der Maschine ist erfolgt.

- NTP Server und Timezone Konfiguration
- Chef Client Konfiguration für Anbindung an Chef Server

Der Server ist damit in die Domain eingebunden. Der Server kennt seine Rolle und kann anhand dieser im folgenden
Provisioning Schritt in den Zielzustand gebracht werden.
