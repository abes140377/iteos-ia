---
title: Walkthrough - Automatisierung Infrastruktur Server
---

# {{ $page.title }}

Der Walkthrough beschreibt die Automatisierung eines Infrastruktur Servers. Infrastruktur Servers stellen grundlegende
Dienste und Services zur Verfügung, die in der Domain benötigt werden.

Arten von Infrastruktur Servers:

- Chef Server
- DNS Server
- LDAP Server
- Gitlab
- Consul
- Vault
- Keycloak
- Sensu Server
- Grafana
- Graylog
- Influxdb
- Kubernetes
- Rancher
