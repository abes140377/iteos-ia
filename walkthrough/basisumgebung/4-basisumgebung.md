---
title: Step 4 - Service start
---

# {{ $page.title }}

![infra-step-4](./images/infra-step-4.png)

Die Dienste eines Infrastruktur Server werden durch den System- und Sitzungs-Manager des Servers verwaltet. Der
vorherige Provision Lauf hat diese eingerichtet, gestartet und so konfiguriert dass die Dienste auch nach einem
Neustart des Servers mit gestartet werden.

Im [Walkthrough - Automatisierung einer Anwendungs Stage](/walkthrough/stage/) finden sie Beispiele für die
Bereitstellung von Diensten mit Docker und der Orchestrierung der Services mit Docker Compose.