---
title: Walkthrough
sidebar: false
---

# {{ $page.title }}

<div style="background-color: #343a40; margin: 20px 0" class="card">
  <div class="card-body">
    <h2 class="card-title">Automatisierung eines Infrastruktur Servers</h2>
    <p class="card-text">
      Automatisierte Bereitstellung eines Infrastruktur Servers für die Entwicklungs Domain.
    </p>
    <a href="basisumgebung/" class="btn btn-primary">
      <i class="far fa-play-circle"></i> Start Walkthrough
    </a>
  </div>
</div>

<div style="background-color: #343a40; margin: 20px 0" class="card">
  <div class="card-body">
    <h2 class="card-title">Automatisierung einer Anwendungs Stage</h2>
    <p class="card-text">
      Automatisierte Bereitstellung einer Anwendungs Stage (dev, qs, staging, prod...) und Installation aller
      Laufzeitkomponeneten als Docker Container für den Betrieb eines Fachverfahrens.
    </p>
    <a href="stage/" class="btn btn-primary">
      <i class="far fa-play-circle"></i> Start Walkthrough
    </a>
  </div>
</div>