---
title: Step 1 - Start Host
---

# {{ $page.title }}

![stage-step-1](./images/stage-step-1.png)

## Kommandos

Host wählen der gestartet werden soll.

``` bash
yade host list
```

Host starten

``` bash
yade host start <hostname>.<domain_name>
```

## Ablauf

- Daten der Maschine werden geladen
- Das durch die Providerzuordnung des Hosts festgelegte Terraform Repository wird ausgeckeckt (GIT clone)
- Terraform wird abhängig von der Umgebung (develop, stage, prod) und dem Provider (Vagrant, vShere, Aws) ausgeführt
- Daten der Maschine werden aktualisiert und gespeichert

## Ergebnis

Die VM für den Host wurde gestartet. Man kann sich mit dem folgenden Kommando auf den Host verbinden:

``` bash
yade host connect <hostname>.<domain_name>
```
