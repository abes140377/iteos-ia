---
title: Walkthrough - Automatisierung Anwendungs Stage
---

# {{ $page.title }}

Bei der Automatisierung der Anwendungs Stage wird ein Server mit allen Laufzeitkomponenten für den Betrieb des Fachverfahrens
bereitgestellt.

Es stehen die folgenden Stages der Fachverfahren zur Verfügung

## SEU 2.5

<table>
  <tr>
    <td><b>Name</b></td>
    <td><b>Host</b></td>
    <td><b>Domain</b></td>
  </tr>
  <tr>
    <td>DEV</td>
    <td>seu25-dev</td>
    <td>dzbw.de</td>
  </tr>
  <tr>
    <td>CI</td>
    <td>seu25-ci</td>
    <td>dzbw.de</td>
  </tr>
</table>

## KM-Ausländer

<table>
  <tr>
    <td><b>Name</b></td>
    <td><b>Host</b></td>
    <td><b>Domain</b></td>
  </tr>
  <tr>
    <td>DEV</td>
    <td>auslaender-dev</td>
    <td>dzbw.de</td>
  </tr>
  <tr>
    <td>CI</td>
    <td>auslaender-ci</td>
    <td>dzbw.de</td>
  </tr>
  <tr>
    <td>QS</td>
    <td>auslaender-qs</td>
    <td>dzbw.de</td>
  </tr>
  <tr>
    <td>MIGRATION</td>
    <td>auslaender-migration</td>
    <td>dzbw.de</td>
  </tr>
  <tr>
    <td>RELEASE</td>
    <td>auslaender-release</td>
    <td>dzbw.de</td>
  </tr>
</table>

## KM-Digant

<table>
  <tr>
    <td><b>Name</b></td>
    <td><b>Host</b></td>
    <td><b>Domain</b></td>
  </tr>
  <tr>
    <td>DEV</td>
    <td>digant-dev</td>
    <td>dzbw.de</td>
  </tr>
  <tr>
    <td>CI</td>
    <td>digant-ci</td>
    <td>dzbw.de</td>
  </tr>
</table>

## KM-Connect

<table>
  <tr>
    <td><b>Name</b></td>
    <td><b>Host</b></td>
    <td><b>Domain</b></td>
  </tr>
  <tr>
    <td>DEV</td>
    <td>connect-dev</td>
    <td>dzbw.de</td>
  </tr>
  <tr>
    <td>CI</td>
    <td>connect-ci</td>
    <td>dzbw.de</td>
  </tr>
</table>

## KM-Meldeportal

<table>
  <tr>
    <td><b>Name</b></td>
    <td><b>Host</b></td>
    <td><b>Domain</b></td>
  </tr>
  <tr>
    <td>DEV</td>
    <td>meldeportal-dev</td>
    <td>dzbw.de</td>
  </tr>
  <tr>
    <td>CI</td>
    <td>meldeportal-ci</td>
    <td>dzbw.de</td>
  </tr>
</table>

## DSDE

<table>
  <tr>
    <td><b>Name</b></td>
    <td><b>Host</b></td>
    <td><b>Domain</b></td>
  </tr>
  <tr>
    <td>DEV</td>
    <td>dsde-dev</td>
    <td>dzbw.de</td>
  </tr>
  <tr>
    <td>CI</td>
    <td>dsde-ci</td>
    <td>dzbw.de</td>
  </tr>
</table>

## eGov

<table>
  <tr>
    <td><b>Name</b></td>
    <td><b>Host</b></td>
    <td><b>Domain</b></td>
  </tr>
  <tr>
    <td>DEV</td>
    <td>egov-ci</td>
    <td>dzbw.de</td>
  </tr>
  <tr>
    <td>CI</td>
    <td>egov-ci</td>
    <td>dzbw.de</td>
  </tr>
</table>
