---
title: Step 3 - Provision Host
---

# {{ $page.title }}

![stage-step-2u3](./images/stage-step-2u3.png)

## Kommando

Host wählen der provisioniert werden soll.

``` bash
yade host list
```

Bei Änderungen am Policyfile des Servers muss die neue Version zunächst auf dem Chef Server installiert werden. Die
Änderungen müssen zuvor auf den GIT Server übertragen werden.

``` bash
yade host cfg policy install <hostname>.<domain_name> --upload
```

Provisioning ausführen

``` bash
yade host provision <hostname>.<domain_name> [--verbose]
```

## Ablauf

- Daten der Maschine werden geladen
- Der chef-client Lauf wird durchgeführt. Der Umfang ergibt sich aus der Rolle und der Umgebung(dev, stage, prod) in der sich der Server befindet.
Anhand dieser Eigenschaften wird die entsprechende Policy vom Chef Server geladen.

## Ergebnis

- Alle in der Policy definierten Cookbook wurden auf dem Server angewendet.
- Der Server befindet sich im deklarierten Zustand.