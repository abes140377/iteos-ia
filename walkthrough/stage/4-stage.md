---
title: Step 4 - Start Stage
---

# {{ $page.title }}

![stage-step-4](./images/stage-step-4.png)

## Kommandos

``` bash
yade host connect <hostname>.<domain_name>
cd ~/projects/<project_id>/src/<project_id>-docker-compose
docker-compose up -d && docker-compose logs -f
```

## Ergebnis

``` bash
open http://auslaender-dev.dzbw.de:7001/console
open http://auslaender-dev.dzbw.de:9200
open http://auslaender-dev.dzbw.de:8161/admin
open http://auslaender-dev.dzbw.de:8888
```

## Deployment der Anwendung

Starten sie den folgenden Jenkins Job

``` bash
open http://auslaender-dev.dzbw.de:8888/view/50.%20KM-Ausl%C3%A4nder%20Release/job/auslaender-dev-environment-stage/build
```
