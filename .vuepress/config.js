module.exports = {
    title: 'ITEOS - AE Infrastruktur Automatisierung',
    description: 'Created with 💚 for DevOps',
    theme: 'default-prefers-color-scheme',
    base: '/iteos-ia/',
    dest: 'public',
    // host: 'localhost',
    extraWatchFiles: [
        'public/*.md',
    ],
    themeConfig: {
        defaultTheme: 'dark',
        repo: 'https://gitlab.com/abes140377/iteos-ia',
        editLinks: true,
        editLinkText: 'Hilf mit diese Seite zu verbessern!',
        smoothScroll: true,
        nav: [
            { text: 'Allgemein', link: '/allgemein/' },
            { text: 'Tools', link: '/tools/' },
            { text: 'Walkthrough', link: '/walkthrough/' },
            { text: 'Konzepte', link: '/konzepte/' },
            { text: 'Links', link: '/links/' },
        ],
        sidebar: {
            '/walkthrough/basisumgebung/': [
                '',
                '1-basisumgebung',
                '2-basisumgebung',
                '3-basisumgebung',
                '4-basisumgebung',
            ],
            '/walkthrough/stage/': [
                '',
                '1-stage',
                '2-stage',
                '3-stage',
                '4-stage',
            ],
        },
    },
    plugins: { '@vuepress/back-to-top': true },
    // When using `light theme` or `dark theme`, you need to add a postcss plugins to your config.js
    postcss: {
        plugins: [
            require('css-prefers-color-scheme/postcss')
        ]
    }
}