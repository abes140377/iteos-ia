---
home: true
actionText: Walkthrough
actionLink: /walkthrough/
footer: ITEOS AöR | Copyright © 2020 Sebastian Freund
---

<div class="features">
    <div class="feature">
        <h2><a href="konzepte/#infrastruktur-as-code"><i class="fas fa-caret-right fa-lg"/>&nbsp;Infrastruktur as Code</a></h2>
        <p>Vorgehen und Konzepte der Infrastruktur Automatisierung mit Terraform</p>
    </div>
    <div class="feature">
        <h2><a href="konzepte/#configuration-management-as-code"><i class="fas fa-caret-right"/>&nbsp;Configuration Management as Code</a></h2>
        <p>Vorgehen und Konzepte des Configuration Management mit Chef</p>
    </div>
    <div class="feature">
        <h2><a href="konzepte/#containervirtualisierung"><i class="fas fa-caret-right"/>&nbsp;Containervirtualisierung</a></h2>
        <p>Vorgehen und Konzepte der Containervirtualisierung mit Docker</p>
    </div>
</div>