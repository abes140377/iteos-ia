---
title: Tools
sidebar: auto
sidebarDepth: 0
---

# {{ $page.title }}

## Grundlagen Toolentscheidung

Grundlage für die Toolentscheidungen waren im Bereich der Anwendungsentwicklung:

- Das was die Tools machen wir in Code oder einer codeähnlichen Sprache z.B. DSL geschrieben

   Tools die nur über eine grafische Oberfläche steuerbar, keine Schnittstellen anbieten und im schlimmsten Fall über
   proprtietäre Protokolle kommunizieren sind für die Verwendung ungeeignet.

- Die Tools sind Open Source, populär und wenn möglich bulletproofed

   Agile Entwicklung und agile Konzepte profitieren stark davon, dass Tools einfach zu verwenden sind.
   Die Tools müssen sofort verwendbar sein. Egal ob zum Testen oder für den produktiven Einsatz. Lange
   Beschaffungszeiten oder ähnliches sind daher nicht erwünscht.

- Verfügen über qualitativ hochwertige Dokumentation und eine große Community

- Unterstützen unterschiedliche Provider(IAC) und laufen auf allen Betriebssystemen (Win, Linux, Osx)

- Bieten die Möglichkeit der Wiederverwendung von Funktionalität (Module) und sind erweiterbar (Plugins)

- Sollten, wenn möglich keine zusätzliche Infrastruktur und wenn doch nur in geringen Umfang benötigen.

- Die Tools setzen die Prinzipien von [Immutable Infrastructure](https://www.digitalocean.com/community/tutorials/what-is-immutable-infrastructure)
um oder unterstützen diese zumindest.

## Eingesetzte Tools

### Infrastructure as code

- [Hashicorp Terraform](https://www.terraform.io/)

   Bietet die Möglichkeit maschinenlesbaren Konfigurationscode für die Bereitstellung von Infrastruktur Komponenten zu
   schreiben. Ziel ist die einheitliche Beschreibung einer Zielinfrastruktur und sorgt dafür, dass diese bei
   unterstützten IaaS-Providern wie gewünscht umgesetzt wird.

### Lifecycle and Configuration Management

- [Puppet](https://puppet.com/)
- [Chef](https://www.chef.io/)
- [Ansible](https://www.ansible.com/)

Bietet die Möglichkeit maschinenlesbaren Konfigurationscode für die Verwaltung von Computersystemen, Servern und
Software zu schreiben. Die Tools ermöglichen eine deklarative Beschreibung der Systeme. Diese Beschreibung dient dann
dazu den gewünschten Systemstatus herzustellen.

Mit dem Konfigurationsmanagement kann man Umgebungen akkurat mit der korrekten Konfiguration und/oder Software replizieren.

So kann man zum Beispiel sicherstellen, dass Test- und Produktionsumgebungen übereinstimmen.

### Continous Integration, Continous Deployment / Delivery

- [Jenkins](https://jenkins.io/)
- [Concourse CI](https://concourse-ci.org/)
- [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/)

#### Continous Integration

Kontinuierliche Integration bzw. permanente Integration aller Bestandteile einer Anwendung.
Dabei werden die in kleineren Schritten vorgenommenen Änderungen (Programmcode einzelner Entwickler) zur bestehende
Anwendung hinzugefügt, um die Auswirkungen auf das Gesamtsystem zu prüfen und die Funktionalität zu testen.

Fokussiert sich auf das Integrieren von Code, sowie die Validierung des Codes auf Unit Tests und eventuell Integration
Test Level. Die Applikation ist dann in einer Testumgebung verfügbar.

#### Continous Deployment / Delivery

Disziplin um bei Bedarf Deployments jeder beliebigen Version der Software per Knopfdruck durchzuführen.
Ermöglicht eine Enge, kollaborative Arbeitsbeziehung zwischen allen, die am Auslieferungsprozess beteiligt sind und ist
damit ein essenzieller Prozess in einer DevOps-Kultur

### Virtualisierung

- [VMware vSphere](https://www.vmware.com/de/products/vsphere.html)
- [Virtualbox](https://www.virtualbox.org/)

Virtualisierung bezeichnet in der Informatik die Nachbildung eines Hard- oder Software-Objekts durch ein ähnliches
Objekt vom selben Typ mithilfe eines Abstraktions-Layers. Dadurch lassen sich virtuelle Geräte oder Dienste wie
emulierte Hardware, Betriebssysteme, Datenspeicher oder Netzwerkressourcen erzeugen.
[Quelle Wikipedia](https://de.wikipedia.org/wiki/Virtualisierung_(Informatik))

### Containervirtualisierung

- [Docker](https://www.docker.com/)

Containervirtualisierung ist eine Methode, um mehrere Instanzen eines Betriebssystems (als sog. „Gäste“) isoliert
voneinander auf einem Hostsystem zu betreiben. Im Gegensatz zur Virtualisierung mittels eines Hypervisors hat
Containervirtualisierung zwar einige Einschränkungen in der Art ihrer Gäste, gilt aber als besonders ressourcenschonend.
[[Quelle Wikipedia]](https://de.wikipedia.org/wiki/Containervirtualisierung)

### Container Orchestrierung

- [Kubernetes](https://kubernetes.io/)
- [Docker Compose](https://docs.docker.com/compose/)

Anwendungen und Umgebungen setzen sich aus Dutzenden oder Hunderten von lose miteinander verknüpften Komponenten
zusammen und sind über unterschiedliche virtuelle und/oder physische Hosts verteilt.
Wo solche Komponenten als Container bereitgestellt werden übernimmt die Container-Orchestrierung deren Verwaltung.
Für gewöhnlich werden die Hosts dazu zu einem Cluster zusammengefasst. Die Orchestrierung übernimmt dabei das Verteilen
der einzelnen Komponenten auf den Hosts des Clusters.
Systeme für die Orchestrierung von Containern ermöglichen sozusagen eine Automatisierung der Bereitstellung, Verwaltung
und Skalierung von Container-basierten Anwendungen.

### Kubernetes Cluster Management

- [Rancher](https://rancher.com/)

   Übernimmt die Verwaltung mehrerer Kubernetes Cluster. Ermöglicht Hochverfügbarkeit von Cluster, eine flexible und
   automatisierte Verwaltung der Hosts Node Management, die Separierung von Umgebungen der Cluster und stellt die
   Benutzerverwaltung zur Verfügung. Eine schickes Webfrontend gibt es gratis dazu.

### Monitoring / Log Management / Tracing

- [Sensu Go](https://sensu.io/)

   Sensu Go widmet sich beim Monitoring den Schwächen, die Nagios und ähnliche Werkzeuge insbesondere bei der
   Überwachung von Cloudinfrastrukturen aufweisen. Mit seiner Form des Monitorings berücksichtigt die Software den
   Anspruch an Skalierbarkeit in der Cloud und unterstützt Trendbeobachtungen durch eine einfache Anbindung von
   Zeitreihendatenbanken. Und auch beim konventionellen Monitoring zeigt Sensu Go seine Stärken.
   [[Quelle Admin Magazin]](https://www.admin-magazin.de/Das-Heft/2019/06/Monitoring-mit-Sensu-Go)

- [Monit](https://mmonit.com/monit/)

   Einfaches Monitoring Werkzeug zum Überwachen von Systemprozessen. Kann Aktinonen ausführen, wenn bestimmte Ereignisse
   auftreten. Zeichnet sich durch eine einfache Installation aus da es keine Abhängigkeiten beispielsweise  zu einer
   Datenbank hat und in den Paketquellen der gängigen Linux Distributionen vorhanden ist.

- [Netdata](https://www.netdata.cloud/)

   Ursprünglich als Lösung für das Monitoring von Echtzeit-Performancedaten entwickelt, hat sich Netdata zur
   Allround-Monitoring-Software gemausert. Weitere Pluspunkte sind die einfache Installation und Konfiguration.
   Bietet eine ansprechende Weboberfläche für die Darstellung von Echtzeitdaten eines Systems.
   [[Quelle Admin Magazin]](https://www.admin-magazin.de/Das-Heft/2018/09/Monitoring-mit-Netdata)

- [Elasticsearch / Logstash / Kibana](https://www.elastic.co/de/)

   Der Elastic Stack (auch bekannt als ELK Stack) wird für eine Vielzahl von Anwendungsfällen eingesetzt, von
   Observability über Sicherheit bis hin zu Enterprise-Suche und Business Analytics.
   [[Quelle elastic.co]](https://www.elastic.co/de/webinars/introduction-elk-stack)

- [Graylog](https://www.graylog.org/)

   Ermöglicht eine einheitliches und systematische Logmanagement von Servern und Anwendungen. Ermöglicht die
   Fehleranalyse und das Alerting auf Basis der ausgewerteten Logereignisse.
   Bietet eine Weboberfläche als zentraler Zugang zu allen Logereignissen einer Umgebung und deren Anwendungen.
   [[Quelle jaxenter.de]](https://jaxenter.de/wissen-sammeln-49947)

- [Grafana](https://grafana.com/)

   Mit Grafana können Metriken abgefragt, angezeigt und alarmiert werden, egal wo sie gespeichert sind. Erstelle,
   erkunde und teile die Dashboards mit deinem Team und fördere eine datenorientierte Kultur.
   [[Quelle netways.de]](https://www.netways.de/monitoring/grafana/)

- [Zipkins](https://zipkin.io/)

   Zipkins is a Java-based application that is used for distributed tracing and identifying latency issues. Unique
   identifiers are attached to each request which are then passed downstream through the different waypoints, or
   services. Zipkin then collects this data and allows users to analyze it in a UI.
   [[Quelle dzone.com Artikel]](https://dzone.com/articles/distributed-tracing-with-zipkin-and-elk)

- [Jaeger](https://www.jaegertracing.io/)

   Jaeger ist eine Open Source-Software, mit der Transaktionen zwischen verteilten Services verfolgt werden können.
   Jaeger wird zur Überwachung und Fehlerbehebung in komplexen Microservice-Umgebungen eingesetzt.
   [[Quelle redhat.com]](https://www.redhat.com/de/was-ist-jaeger)

### Webserver / Reverse Proxy

- HAProxy
- Apache
- Nginx
- Traefik

### Service Registry / Key Value Stores / Konfigurations DB

- [Hashicorp Consul](https://www.consul.io/)
- [Etcd](https://etcd.io/)

Ermöglichen die automatische Erkennung von Diensten in einem Rechnernetz. Hierbei kommen Kommunikationsprotokolle zum
Einsatz, welche beschreiben, wie sich die Dienste finden, um miteinander kommunizieren können.
[[Quelle Wikipedia]](https://de.wikipedia.org/wiki/Service-Discovery)

Beide Tools bieten eine Key-Value Datenbank. Werte können einfach über das HTTP gespeichert und gelesen werden.

Etcd ist besonders durch seinen Einsatz als Configuration Backend im Kubernetes Umfeld bekannt.

### Identity and Access Management

- [Keycloak](https://www.keycloak.org/)

   Keycloak is an open source software product to allow single sign-on with Identity Management and Access Management
   aimed at modern applications and services
   [[Quelle Wikipedia]](https://en.wikipedia.org/wiki/Keycloak)

### Secrets Management

- [Hashicorp Vault](https://www.vaultproject.io/)

   Das Secret-Sharing eignet sich, um gemeinsame Zugänge zu Benutzerkonten oder Diensten etwa für Admin-Teams zentral
   zu verwalten. Zur technischen Umsetzung dieses Konzepts für eigene Unternehmensdienste gibt es jedoch nur eine
   Handvoll Werkzeuge, die sich bewährt haben. Hashicorp Vault ist eines davon.
   [[Quelle Admin Magazin]](https://www.admin-magazin.de/Das-Heft/2016/10/Credential-Verwaltung-mit-Hashicorp-Vault)

### Datenbanken

- Oracle
- SQL Server
- Postgres
- Mysql / MariaDB
- MongoDB

### Team / Kommunikation / Sonstige

- [Gopass](https://www.gopass.pw/)

   Password Management für Teams

- [Rocker Chat](https://rocket.chat/)

   Free, Open Source, Enterprise Team Chat

- [Minio](https://min.io/)

   High Performance, Kubernetes-Friendly Object Storage

- [Rundeck](https://www.rundeck.com/)

   Rundeck ist ein Open-Source Automatisierungstool für den Einsatz in Rechenzentren oder Cloud-Umgebungen

- [Sonarqube](https://www.sonarqube.org/)

   SonarQube ist eine Plattform für die statische Analyse und Bewertung der technischen Qualität von Sourcecode.
   SonarQube analysiert den Sourcecode hinsichtlich verschiedener Qualitätsbereiche und stellt die Ergebnisse über eine
   Website dar.
   [[Quelle Wikipedia]](https://de.wikipedia.org/wiki/SonarQube)

### Server Orchestration / Job Execution

- [Bold](https://puppet.com/docs/bolt/latest/bolt.html)

   Bolt is an open source orchestration tool that automates the manual work it takes to maintain your infrastructure.
   Use Bolt to automate tasks that you perform on an as-needed basis or as part of a greater orchestration workflow.
   For example, you can use Bolt to patch and update systems, troubleshoot servers, deploy applications, or stop and
   restart services. Bolt can be installed on your local workstation and connects directly to remote targets with SSH
   or WinRM, so you are not required to install any agent software.
   [[Quelle Bold Homepage]](https://puppet.com/docs/bolt/latest/bolt.html)

### Workflow Engines / Decission Automation

- [Cloudslang](https://cloudslang.io/)
- [Activity](https://www.activiti.org/)
- [Camunda](https://camunda.com/de/)

### Caching

- Hazelcast
- Memcached
- Redis

### Messaging

- RabbitMQ
- ActiveMQ
- Kafka

## Eingesetzte Programmiersprachen

- Java
- Groovy
- Kotlin
- Golang
- Ruby
- Python
- Bash

## Linux

### Linux Distributionen

- Ubuntu Server 16.04, 18.04

### Docker Basis Images

- Oracle Linux
- Centos
- Debian
- Alpine
