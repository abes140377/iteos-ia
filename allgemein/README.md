---
title: Allgemein
sidebar: auto
sidebarDepth: 0
---

# {{ $page.title }}

## Übersicht

Das Bereitstellen verlässlicher Infrastruktur ist aufwendig, kann sehr aufreibend sein und erfordert einen hohen
Zeitaufwand.

Im Zeitalter von DevOps versprechen einem jede Menge Tool und Konzepte diese Punkte zu vereinfachen. Einzeln betrachtet
und auf Basis der üblichen "Hello World" Beispiele kann man sich schnell überzeugen lassen.

Doch wie sieht die Realität aus?

Diese Seite versucht zumindest die Realität der Anwendungsentwicklung Iteso zum Thema zu beschreiben.

Alle Betrachtungen basieren auf den Erfahrungen der letzten 1,5 - 2 Jahre in denen wir uns im Bereich der AE mit diesem
Thema auseinandergesetzt haben.

Nach dieser Zeit zeigt sich bereits deutlich Vorteile. Trotz der Aufwände für den Aufbau der Umgebung für die
Infrastruktur Automatisierung haben sich die Konzepte und das Vorgehen zur Umsetzung bewährt.

## Bereitstellung von Infrastruktur

### Zeitlicher Aufwand

Die folgende Tabelle versucht eine ungefähre Einordnung der zeitlichen Aufwände für die automatisierte Bereitstellung
einer kleine Microservice Anwendung auf der Public Cloud von AWS. Es geht hierbei nicht um verlässliche oder
allgemeingültige Zeitangaben, sondern darum zu zeigen wie sich der Aufwand je nach Umfang entwickelt.

Die zeitlichen Angaben basieren auf den folgenden Voraussetzungen:

- Volle Handlungsfähigkeit einer Einzelperson in allen Bereichen ohne zeitliche Verzögerung
- Tagespensum 4-6 Stunden
- Keine bis mäßige Erfahrung bei den eingesetzten Tools.
- Fortgeschrittene Programmierkenntnisse in den relevanten Programmiersprachen und Erfahrung in der Linux Server
Administration waren vorhanden.

| Projekte                       | Beispiel                                         | Aufwand      |
|--------------------------------|--------------------------------------------------|--------------|
| Managed Service                | ECS, ELB, RDS, ...                               | 1 - 2 Wochen |
| Distributed System (stateless) | Microservice Anwendung (Gateway + Microservices) | 4 Wochen     |
| Distributed System (satefull)  | Elasticsearch, Kafka, MongoDB                    | 2 Monate     |
| Gesamte Cloud Infrastruktur    | Apps, DBs, CI/CD, Monitoring, Logging, Tracing   | 6 Monate     |

### Warum dauert das so lange

#### Yak shaving [?](https://en.wiktionary.org/wiki/yak_shaving)

Eine schier endlose Liste von Aufgaben die erledigt werden muss bevor man das machen kann, was man eigentlich will.

#### Die Checkliste ist lang

Als Neueinsteiger in das DevOps Umfeld hätte man vielleicht erwartet, dass die Aufwände geringer wären. Die Liste der zu
erledigenden Aufgaben jedoch ist lang.

| Aufgabe            | Beschreibung                                                                                                                       | Tools                                                            |
|--------------------|------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------|
| Install            | Installation aller Software und den Abhängigkeiten                                                                                 | Skripte, Configuration Management Tool                           |
| Configure          | Konfiguration der Software: Ports, Dateipfade,  Benutzer, SSH Keys, Replikation, ...                                               | Configuration Management Tool                                    |
| Provision          | Provisionierung der Infrastruktur: VM Instanzen, Lastverteilung, Netzwerk Topologie, Security Groups,  Permissions / Firewall, ... | Terraform, CloudFormation                                        |
| Deployment         | Bereitstellen der Services auf der Infrastruktur, Roll out,  Update Strategie z. B.: blue-green, rolling, canary                   | Scripts, Orchestration tools  (ECS, K8s, Nomad)                  |
| Security           | Transportverschlüsselung, Authentifizierung, Authorisierung,  Secrets Management, Server Hardening                                 | ACM, EBS Volumes, Vault CIS                                      |
| Monitoring         | Availability Metriken, Business Metriken, APM (Anwendungs Metriken), Server Metriken, Events, Observability, Tracing, Alerting     | CloudWatch, Sensu, Moskito, Time Series DB, Dashboards, Realtime |
| Logs               | Log Rotation, Zentrales Management und Aggregation, Alerting                                                                       | CloudWatch Logs, ELK, Graylog, Shippers                          |
| Backup / Restore   | Replikation, Backup sensibler Daten, Chaos Recovery, zeitgesteuert                                                                 | RDS, ElastiCache                                                 |
| Networking         | VPCs, Subnets, Static / dynamic IP's, Service Discovery, Service Mesh, Firewall, DNS, SSH Zugriffe, VPN access, Bastion Hosts      | EIPs, VPCs, NACLs, Route 53, Consul,  OpenVPN                    |
| Hochverfügbarkeit  | Ausfälle einzelner Services, VMs oder Availability Zones und Regions                                                               | Multi AZ, Multi Region, Replication,  ASGs, ELBs                 |
| Skalierbarkeit     | Lastabhängige Aufwärts / Abwärts Skalierung, horizontal oder vertikal                                                              | ASGs, Replication, Sharding, Caching, Divide and Conquer         |
| Performance        | CPU, Memory, Disk, Network, GPU, Query tuning, Benchmarking,  Load Testing, Profiling                                              | Moskito, Valgrind, VisualVM, Gatling                             |
| Optimierung Kosten | Auswahl VM Instanzen, Auto Scaling, Abschalten nicht benötigter Resourcen                                                          |                                                                  |
| Dokumentation      | Code, Architekture und Best Practices dokumentieren                                                                                | READMEs, Wiki, Chat                                              |
| Tests              | Automatisierte Tests schreiben, regelmäßig Testen, Code Änderungen führen Test automatisch aus, Code Review, 4 Augen Prinzip       | Terratest, Serverspec, Test Kitchen, dgoss                       |

Vielleicht ist nicht jeder Punkt zwingend erforderlich, um Infrastruktur zu automatisieren. Eine Checkliste wie diese
Hilft jedoch dabei den Umfang und die Aufgaben festzulegen. Der schlimmste Fall wäre, dass ein essenzieller Punkt
vergessen wurde und erst in Produktion gemerkt wird, dass es ohne nicht geht. Das zerstört gerade bei jungen Projekten
und neuen Technologien und Konzepten sofort die Akzeptanz.

### Automatisierte Bereitstellung

Eine Voraussetzung für die automatisierte Bereitstellung von Infrastruktur ist dass (beinahe) alles Code ist.

Zum Einsatz kommen die folgenden Konzepte:

- Manuelle Bereitstellung Compute Resourcen &#8680; Infrastructure as Code
- Manuelle Server Konfiguration &#8680; Configuration Management
- Manuelle Anwendungs Konfiguration &#8680; Konfigurations Dateien und Konfigurations Backends
- Manuelle Builds &#8680; Continous Integration
- Manuelle Deployments &#8680; Continous Deployment / Continous Delivery
- Manuelles Testen &#8680; Continous Testing
- Manuelle DBA Aufgaben &#8680; Automatisierte Schema Migration und Bereitstellung von Daten
- Fest kodiert Konfiguration &#8680; Auslagern in geeignete Konfiguration Backends und Abfrage dieser, wenn benötigt

### Vorteile eines Code-zentrierten Ansatz

1. Automatisierbar
2. Versions Kontrolle
3. Gegenseitige Überprüfung (Code Review), Pull Requests, Diskussion
4. Testbar
5. Dokumentation
6. Wiederverwendung

### Was haben wir davon?

- Die Zeitaufwände für die Bereitstellung lassen sich dramatisch reduzieren.

   Ein Beispiel in dem der aktuelle Umfang der Automatisierung für die AE internen Projekt Stages genutzt wird ist
   KM-Ausländer. Die erforderlichen Laufzeitkomponenten für den Betrieb der KM-Ausländer Anwendung sind:
   * Weblogic Server im CLuster Betrieb mit einem Admin Server und einem Managed Server
   * Oracle oder SQLServer Datenbank Server mit 5 Schemata / Benutzer
   * Openldap Server
   * Elasticsearch Server
   * Logstash Shipper
   * ActivMQ Server

   Für die Bereitstellung dieser Umgebung vom Entstehen des Virtuellen Maschine im vSphere bis zur aufrufbaren Anwendung
   im Webbrowser benötigen wir ca. 1-2 Stunden. Es sind nur minimale händische Anpassungen nach dem Start des Linux
   Systems notwendig, die lediglich dem Umstand geschuldet sind, dass die VM Vorlage nicht optimal passt.
   Das gesamte Bootstraping und das Provisioning des Grundsystems können von der Kommandozeile ausgeführt werden.
   Die Laufzeitkomponenten werden als Docker Container betrieben. Das Deployment der Anwendung und aller für den Betrieb
   erforderlichen Daten werden über einen Jenkins Job durchgeführt.

   Im AE Umfeld des Autors werden quasi ausschließlich JAVA EE Anwendungen entwickelt und für die Entwicklerteams
   bereitgestellt. Technisch und konzeptionell unterscheiden sich die unterschiedlichen Fachverfahren nur geringfügig.
   Für die automatisierte Bereitstellung der Umgebungen bedeutet dies, dass wesentliche Teile wiederverwendet werden
   können. Die erforderliche Zeit z. B.: in die Bereitstellung von Docker Container für ein anderes Verfahren reduziert
   sich auf einen Bruchteil der Zeit die für die initiale Erstellung benötigt wurde. Docker ist hier nur ein Beispiel.
   Dasselbe erreicht man durch den Einsatz geeigneter Konzepte und Techniken auch ohne Docker.

## Auswirkungen

Wir mussten viele neue Dinge lernen und konnten uns intensiv mit aktuellen Konzepten und Technologien auseinandersetzen.
Wir mussten uns freimachen von alteingesessenen Vorgehensweisen und neu Paradigmen verstehen und adaptieren.
Wir mussten aus den einzelnen Building Blocks ein tragfähiges Gesamtsystem erstellen, dass alle unsere Anforderungen
erfüllen konnte. Hier entsteht ein hoher integrativer Aufwand der als Unterbau erstmal keinerlei direkten Nutzen für
die Anwendungsentwicklung in den Projekten bringt.
Wir mussten uns für einen Toolstack entscheiden. Im Nachhinein betrachtet ist jedoch nicht die Toolentscheidung die
Garantie für den Erfolg, sondern eher die zuvor genannten Punkte.

---

**Vorher: Änderung direkt durchführen**

![from-to-1](./images/from-to-1.png)

**Nachher: Änderung indirekt und automatisch**

![from-to-2](./images/from-to-2.png)

---

**Problem: Ich kann doch mal kurz was direkt ändern...**

![from-to-3](./images/from-to-3.png)

**Auswirkung: Die Tools funktionieren nicht mehr**

![from-to-4](./images/from-to-4.png)

---

**Vorher: Skaliert schlecht**

![from-to-5](./images/from-to-5.png)

**Nachher: Skaliert gut**

![from-to-6](./images/from-to-6.png)

---
