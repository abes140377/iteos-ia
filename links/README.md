---
title: Links
sidebar: auto
sidebarDepth: 0
---

# {{ $page.title }}

## YouTube

### Building Automated Pipelines for Infrastructure Code with Terraform and Packer

<div style="margin-top: 10px;" class="video-block">
    <iframe max-width=100% height=auto
      src="https://www.youtube.com/embed/4uxUScFWzPc" frameborder="0"
      allow="encrypted-media"
      allowfullscreen>
    </iframe>
</div>
